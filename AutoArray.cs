﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace ArrayApp {
    public class ResizeArgs : EventArgs {
        public readonly uint size;

        public ResizeArgs(uint size) {
            this.size = size;
        }
    }

    public class AutoArray<T> {
        private T[] data;

        public delegate void ResizeEventHandler(Object sender, ResizeArgs resizeArgs);
        public event ResizeEventHandler Resize;

        public uint Size {
            get; private set;
        }

        public uint Length {
            get; private set;
        }

        public T DefaultValue {
            get; set;
        }

        public AutoArray(T defaultValue) {
            Size = 4;
            Length = 0;
            DefaultValue = defaultValue;
            data = new T[Size];
        }

        private bool VerifyMinIndex(int index) {
            return (index >= 0);
        }

        private bool VerifyMaxIndex(int index) {
            return (index < Length);
        }

        private bool VerifyCapacity(int index) {
            return (index < Size);
        }

        private void Increase() {
            Size *= 2;
            Array.Resize<T>(ref data, (int)Size);
            onResize();
        }

        private T GetItem(int index) {
            Monitor.Enter(this);
            try {
                if (VerifyMinIndex(index) && VerifyMaxIndex(index)) {
                    return data[index];
                }
                else {
                    throw new IndexOutOfBoundsException();
                }
            }
            finally {
                if (Monitor.IsEntered(this)) {
                    Monitor.Exit(this);
                }
            }

        }

        private void SetItem(int index, T value) {
            Monitor.Enter(this);
            try {
                if (VerifyMinIndex(index)) {
                    while (!VerifyCapacity(index)) {
                        Increase();
                    }
                    while (Length < index) {
                        data[Length] = DefaultValue;
                        ++Length;
                    }
                    data[index] = value;
                    if (index == Length) {
                        ++Length;
                    }
                }
            }
            finally {
                if (Monitor.IsEntered(this)) {
                    Monitor.Exit(this);
                }
            }
        }

        public T this[int index] {
            get {
                return GetItem(index);
            }
            set {
                SetItem(index, value);
            }
        }

        public TimeSpan Add(T value) {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            Monitor.Enter(this);
            try {
                watch.Stop();
                SetItem((int)Length, value);
            }
            finally {
                if (Monitor.IsEntered(this)) {
                    Monitor.Exit(this);
                }
            }
            return watch.Elapsed;
        }

        public bool TryAdd(T value) {
            if (Monitor.TryEnter(this)) {
                try {
                    SetItem((int)Length, value);
                }
                finally {
                    if (Monitor.IsEntered(this)) {
                        Monitor.Exit(this);
                    }
                }
                return true;
            }
            return false;
        }

        public override string ToString() {
            StringBuilder dataSB = new StringBuilder();
            try {
                Monitor.Enter(data);
                for (uint i = 0; i < Length; i++) {
                    dataSB.AppendLine("\t[" + i + "]\t" + data[i].ToString());
                }
            }
            finally {
                if(Monitor.IsEntered(data)) {
                    Monitor.Exit(data);
                }
            }
            return dataSB.ToString();
        }

        public void saveToFile(string path) {
            File.WriteAllText(path, this.ToString());
        }

        private void onResize() {
            Resize?.Invoke(this, new ResizeArgs(Size));
        }
    }

    public class IndexOutOfBoundsException : Exception {
        public IndexOutOfBoundsException() {}
        public IndexOutOfBoundsException(string msg) : base(msg) {}
        public IndexOutOfBoundsException(string msg, Exception inner) : base(msg, inner) {}
    }
}

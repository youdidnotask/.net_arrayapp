﻿using System;
using System.Diagnostics;
using System.Threading;

namespace ArrayApp {
    class Communicator {
        public void Communicate(Object sender, ResizeArgs resizeArgs) {
            Console.WriteLine("\t[RESIZE! Array size: {0}]", resizeArgs.size);
        }
    }

    class Client<T> {
        private AutoArray<T> autoArray;

        public Client(AutoArray<T> autoArray) {
            this.autoArray = autoArray;
        }

        private void WaitToAdd(T value) {
            TimeSpan time = autoArray.Add(value);
            Console.WriteLine("[{0}] Access time: {1}", Thread.CurrentThread.ManagedThreadId, time);
        }

        private void AddWithoutWait(T value) {
            if(autoArray.TryAdd(value)) {
                Console.WriteLine("[{0}] Succes", Thread.CurrentThread.ManagedThreadId);
            }
            else {
                Console.WriteLine("[{0}] Fail", Thread.CurrentThread.ManagedThreadId);
            }
        }

        private void GetContent() {
            System.Console.WriteLine("[{0}] Content request\n{1}", Thread.CurrentThread.ManagedThreadId, autoArray.ToString());
        }

        private void getValue(int index) {
            Console.WriteLine("[{0}] Value request autoArray[{1}] = {2}", Thread.CurrentThread.ManagedThreadId, index, autoArray[index]);
        }

        private void setValue(int index, T value) {
            autoArray[index] = value;
        }

        private void save() {
            autoArray.saveToFile("data.txt");
        }

        public void RunOption_01(Object value) {
            Console.WriteLine("Option_01 is running");            
            WaitToAdd((T)value);
            AddWithoutWait((T)value);
            WaitToAdd((T)value);
            getValue(1);
            WaitToAdd((T)value);
            setValue(9, (T)value);
            AddWithoutWait((T)value);
            GetContent();
        }
        public void RunOption_02(Object value) {
            Console.WriteLine("Option_02 is running");
            AddWithoutWait((T)value);
            AddWithoutWait((T)value);
            WaitToAdd((T)value);
            WaitToAdd((T)value);
            setValue(22, (T)value);
            WaitToAdd((T)value);
            getValue(5);
            GetContent();
        }
        public void RunOption_03(Object value) {
            Console.WriteLine("Option_03 is running");
            WaitToAdd((T)value);
            getValue(0);
            AddWithoutWait((T)value);
            AddWithoutWait((T)value);
            setValue(3, (T)value);
            WaitToAdd((T)value);
            WaitToAdd((T)value);
            setValue(11, (T)value);
            WaitToAdd((T)value);
            GetContent();
            getValue(4);
            save();
        }
    }

    class Program {
        static void Main(string[] args) {
            try {
                AutoArray<string> autoArray = new AutoArray<string>("none");

                Communicator comm = new Communicator();
                autoArray.Resize += comm.Communicate;

                Client<string> client = new Client<string>(autoArray);
                Thread clientThread_01 = new Thread(new ParameterizedThreadStart(client.RunOption_01));
                Thread clientThread_02 = new Thread(new ParameterizedThreadStart(client.RunOption_02));
                Thread clientThread_03 = new Thread(new ParameterizedThreadStart(client.RunOption_03));

                clientThread_01.Start("A");
                clientThread_02.Start("B");
                clientThread_03.Start("C");

                clientThread_01.Join();
                clientThread_02.Join();
                clientThread_03.Join();

                Console.Write("\nMission completed");
            }
            catch (IndexOutOfBoundsException) {
                Console.WriteLine("ERROR: Index out of bounds.");
            } finally {
                Console.ReadKey();
            }

        }

        private static void GetInfo(AutoArray<string> autoArray) {
            Console.WriteLine("SIZE: " + autoArray.Size + "\n" +
                                "NUMBER OF ELEMENTS: " + autoArray.Length + "\n" +
                                "CONTENT:\n" + autoArray.ToString());
        }

        private static void GetContent(AutoArray<string> autoArray) {
            Console.WriteLine("CONTENT:\n" + autoArray.ToString());
        }
    }
}
